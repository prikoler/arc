## ARC

- Описать несколько структур – любой легковой автомобиль и любой грузовик. Структуры должны содержать марку авто, год выпуска, объем багажника/кузова, запущен ли двигатель, открыты ли окна, заполненный объем багажника
- Описать перечисление с возможными действиями с автомобилем: запустить/заглушить двигатель, открыть/закрыть окна, погрузить/выгрузить из кузова/багажника груз определенного объема
- Добавить в структуры метод с одним аргументом типа перечисления, который будет менять свойства структуры в зависимости от действия
- Инициализировать несколько экземпляров структур. Применить к ним различные действия. Положить объекты структур в словарь как ключи, а их названия как строки например var dict = [structCar: 'structCar']
- Почитать о Capture List и описать своими словами и сделать скрин своего примера и объяснения Capture
- У нас есть класс мужчины и его паспорта. Мужчина может родиться и не иметь паспорта, но паспорт выдается конкретному мужчине и не может выдаваться без указания владельца. Чтобы разрешить эту проблему, ссылку на паспорт у мужчины сделаем опциональной, а ссылку на владельца у паспорта – константой. Также добавим паспорту конструктор, чтобы сразу определить его владельца. Таким образом, человек сможет существовать без паспорта, сможет его поменять или выкинуть, но паспорт может быть создан только с конкретным владельцем и никогда не может его сменить. Повторить все что на черном скрине и решить проблему соблюдая все правила!
